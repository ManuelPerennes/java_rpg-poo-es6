import Ennemy from '/class/ennemy.js';
export default class Berserker extends Ennemy {
    constructor(name) {
        super(name);
        this.health *= 1.5;
    };
}