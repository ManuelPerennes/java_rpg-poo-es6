import Ennemy from '/class/ennemy.js';
export default class Assassin extends Ennemy {
    constructor(name) {
        super(name);
        this.assassinat = 'assassinat';
    };

    attack(cible) {
        const newhealth = cible.getHealth() - this.hitStrength;
        this.hitStrength *= 1.1;
        cible.setHealth(newhealth);
    }
}
// L'Assassin inflige 10% de degat supplementaire à chaque nouvelle attaque